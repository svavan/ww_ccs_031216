#include <main.h>

//������� ������������ � �������� ����.

//#device adc=8

#FUSES WDT                      //Watch Dog Timer
#FUSES INTRC_IO                 //Internal RC Osc, no CLKOUT
#FUSES NOCPD                    //No EE protection
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOMCLR                   //Master Clear pin used for I/O
#FUSES PUT                      //Power Up Timer
//#FUSES NOBROWNOUT               //No brownout reset          

//#use delay(clock=4000000, RESTART_WDT)

//�� ����������� ������ ��� �����������
//����� �����-������ ��� ���������� ���������                          
#use fast_io(A) 

//������ ����� �����-������
#byte GPIO   = 0x05
//������ �������� ����������� 
//����� �����-������
#byte TRISIO = 0x85
//����� �������� �����������
#byte CMCON  = 0x19
//������ �������� ����������
#byte PIR1   = 0x0C

//�����, � ������� ���������
//��������� 1, 2 � ����������
#bit  LED1 = GPIO.5
//#bit  LED2 = GPIO.4
//������ "��������� ����"
//#bit  BUTTON = GPIO.4

#bit  SOUND  = GPIO.4
#bit  ExOut  = GPIO.3

#bit  COMPAR = CMCON.6
#bit  CMIF   = PIR1.3

//(C) sergeyk.kiev.ua

//unsigned char mute;

void Delay_s(unsigned char sec)
{
while (sec>0) 
   { Delay_ms(250);
     Delay_ms(250);
     Delay_ms(250);
     Delay_ms(250);
     sec--;
   }
}

#define flashOn  40
#define flashOff 80

/*void LED1flash(void)
{
   unsigned char flash;
   flash=5;
   while (flash>0)
   { LED1=1;
      delay_ms(flashOn);
      LED1=0;
      delay_ms(flashOff);
      flash--;
   }
}*/

void Alarm(unsigned char flash)
{
   //unsigned char flash;
   //flash=5;
   while (flash>0)
   { 
      LED1=1;
      
      //delay_ms(flashOn);
      for (int counter = 0; counter < 100; counter ++)
      {
         sound = 1;
         delay_ms(1);
         sound = 0;
         delay_ms(1);       
      };   
      LED1=0;
      delay_ms(flashOff);
      flash--;
   }
}

/*void LED2flash(void)
{
unsigned char flash;
flash=5;
while (flash>0)
  { LED2=1;
    delay_ms(flashOn);
    LED2=0;
    delay_ms(flashOff);
    flash--;
  }
}*/


/*void BeepBeep(void)
{
// ���� ������ �������� � �� ������ 
// ������ ���������� �����, �� ������ 
// �������� ������ SOS
if ((COMPAR==0)&&(mute==0)) SOUND=1; //�������� ������
delay_ms(120); // ����� 120 ��
SOUND=0; // ��������� ������
delay_ms(60); // ����� 60 ��
if ((COMPAR==0)&&(mute==0)) SOUND=1;
delay_ms(90);
SOUND=0;
delay_ms(60);
if ((COMPAR==0)&&(mute==0)) SOUND=1;
delay_ms(150);
SOUND=0;
}*/



void main(void)
{
//unsigned char wait,alarm;
// ��������� ����� �����-������
TRISIO=0b00001010; // GP1, GP3 - ����, ��������� - �����

// ������ 0 ������������� �� ������ � WDT
// � ������������ ��������� (2.304 �������)
setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
setup_wdt(WDT_2304MS);

//������ 1 ��������
setup_timer_1(T1_DISABLED);

//��� ��������
setup_adc_ports(NO_ANALOGS|VSS_VDD);
setup_adc(ADC_OFF);

// �������������� � ccs 5.045
//����������: ����������� ����� ��������� �
//��������� �������� ����������; ���������
//����� ��������� � ����� GP1; ����� ����������� 
//����� ������� �� ����� GP2 (��� ������� �������)
  setup_comparator(A1_VR_OUT_ON_A2); //|COMP_INVERT
//   setup_comparator(A0_A1_OUT_ON_A2);

//��������� ���������� ��������� �������� ����������
setup_vref(VREF_LOW|5);

//������������� ��������� ������ �����-������ ����������
PORT_A_PULLUPS(false);

//���������� �� �����������
//enable_interrupts(INT_COMP);
//��������� ����������
//enable_interrupts(GLOBAL);

//�������� ����
Alarm(2);


while(1){
   LED1=SOUND=ExOut=0;
   //LED1Flash();

   if (COMPAR==1)
      { 
         //��������� �� ���
         setup_comparator(NC_NC_NC_NC); //��������� ����������
         setup_vref(FALSE); //��������� �������� �������� ����������
         //  TRISIO=0b11111111; // ��� ����� �� ����
         LED1=SOUND=ExOut=0; // ��������� ��� �������
 
         sleep(); //��������, ���� WDT �� ��������
         sleep(); //�������� ��������, ��� ������� ������
         sleep(); //�������� �� 3*2.304 �������

         //LED1=LED2=SOUND=0; // ��������� ��� �������

         //�����������
         //  TRISIO=0b00001010;
         //A1_VR_OUT_ON_A2
         setup_comparator(A1_VR); //�������� ����������
         setup_vref(VREF_LOW|5); //�������� �������� �������� ����������
         LED1=1;
         delay_ms(100); // �����, ����� ��� ������������
      } 

//��������� ������
   if (COMPAR==0) //���� ����
      { 
                   LED1=1;
                   
                   delay_ms(100);
                   
                   if (COMPAR==0)
                   { 
                     delay_ms(100);
                     
                     if (COMPAR==0)
                        {
                           ExOut=1;            
                           Alarm(5);
                           //delay_ms(25);
                        }
                   }
      }
   }
}

//������������� ��������� ��� ��������� ����������
//������� ������� �������� � ������� �� � ������� ����:
//#rom 0x3FF = {0x343C}
